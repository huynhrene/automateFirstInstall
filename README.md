# Collection de scripts bash à exécuter sur une distribution Linux

## Installation / Exécution de script

* Mise à jour : 
```
curl -sL https://framagit.org/rene/automateFirstInstall/raw/master/Tools/update.sh | bash -
```

* Nettoyage après mise à jour : 
```
curl -sL https://framagit.org/rene/automateFirstInstall/raw/master/Tools/clean_up.sh | bash -
```

* Installation de Oh-My-Zsh : 
```
curl -sL https://framagit.org/rene/automateFirstInstall/raw/master/Tools/oh-my-zsh.sh | bash -
```

* Nettoyage espace disque : 
```
curl -sL https://framagit.org/rene/automateFirstInstall/raw/master/Tools/clean_free_space.sh | bash -
```
