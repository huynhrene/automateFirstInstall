# Mise à jour dépôts
apt-get update

# Mise à jour de paquets
apt-get upgrade -y 
apt-get dist-upgrade -y

# Suppression de paquets
apt-get autoremove

# Suppression de paquets d'installation
apt-get -y clean
apt-get -y autoclean

