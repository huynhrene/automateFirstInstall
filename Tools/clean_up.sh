#!/bin/bash
# Exit immediately if a command exits with a non-zero status
set -e

echo "Cleaning Up" 
apt-get -f install
apt-get autoremove
apt-get -y autoclean
apt-get -y clean