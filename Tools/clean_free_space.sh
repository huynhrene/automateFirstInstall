#!/bin/bash
# Exit immediately if a command exits with a non-zero status
set -e

echo "
---------------------------------
Script de nettoyage
---------------------------------
"
apt-get autoremove
echo "
--------------------------------------------------------------
Les paquets inutiles ont été supprimés
--------------------------------------------------------------
"
apt-get clean
apt-get autoclean
echo "
-------------------------------------
apt-get a été nettoyé
-------------------------------------
"
find ~/.thumbnails -type f -atime +7 -exec rm {} ;
echo "
--------------------------------------------------------
Miniatures des images supprimées
--------------------------------------------------------
"
rm -r -f ~/.local/share/Trash/files/*
echo "
---------------------------------------
La corbeille a été vidée
---------------------------------------
"
find ~/ -name '*~' -exec rm {} ;
echo "
----------------------------------------------------------------------------------------------------------
Les fichiers temporaires (terminant par ~) du dossier /home ont été supprimés
----------------------------------------------------------------------------------------------------------
"
echo "
---------------------
|Nettoyage terminé !|
---------------------
"